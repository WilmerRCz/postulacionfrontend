import { Component, OnInit } from '@angular/core';
import { PeticionesService } from 'src/app/services/peticiones.service';


@Component({
  selector: 'app-tabla1',
  templateUrl: './tabla1.component.html',
  styleUrls: ['./tabla1.component.css'],
  providers: [PeticionesService]
})
export class Tabla1Component implements OnInit {

  public numeros: any;
  
  
    
  constructor(
    private _peticionesServices: PeticionesService
  ) { 
    
   }

  ngOnInit() {
    this._peticionesServices.getNumber().subscribe(
      result=>{
        this.numeros = result.data;
      },
      error=> {
        console.log(<any>error);
      }
    )
    
  }

  getOcurrencia(){
    let arregloConRepetidos = this.numeros;
    let sinRepetidos = arregloConRepetidos.filter((valor, indiceActual, arreglo) => arreglo.indexOf(valor) === indiceActual);
    return sinRepetidos;

  }

  }

